<?php session_start();
include("head.php");

if (empty($_SESSION) or $_SESSION['connecte'] != true) :
    include("header.php");
    echo "Vous ne devriez pas être ici : <a href='index.php'>Retour</a>";
else :
    include('connexionbdd.php');
    include("header.php");
    include("nav.php");
    ?>

    <h1 class='h1-qcm'>Quelques QCM clé en main</h1>

	<p>Pour chaque QCM proposé sur cette page, vous pouvez :</p>
	<ul>
	<li>Sélectionner puis copier la liste des numéros de questions pour ensuite l'utiliser via le menu <strong>[Créer un QCM/ A partir d'une liste]</strong></li>
	<li>Générer directement le QCM correspondant en cliquant sur le bouton de génération correspondant.</li>
    </ul>
	<br>

    <ul>
        <li>
            <h3>QCM sujet zéro - juillet 2019</h3>
            <p>191;192;193;194;195;196;215;145;216;217;218;219;149;224;225;226;142;227;260;261;241;242;243;262;228;229;230;231;232;233;208;209;210;143;211;212;197;198;200;199;201;202</p>
            <form method="post" action="qcm-valide-cle.php">
			<input type="hidden"  
			value="191;192;193;194;195;196;215;145;216;217;218;219;149;224;225;226;142;227;260;261;241;242;243;262;228;229;230;231;232;233;208;209;210;143;211;212;197;198;200;199;201;202" 
			name="cle" >
			<button class='btn btn-info'>Générer le QCM sujet zéro - juillet 2019</button>
			</form>
		</li>
        <br>
        <li>
            <h3>QCM sujet zéro initial - mai 2019</h3>
            <p>220;94;221;222;28;32;223;234;235;263;264;265;8;10;30;205;206;207;203;204;214</p>
            <form method="post" action="qcm-valide-cle.php">
			<input type="hidden"  
			value="220;94;221;222;28;32;223;234;235;263;264;265;8;10;30;205;206;207;203;204;214" 
			name="cle" >
			<button class='btn btn-info'>Générer le QCM sujet zéro initial- mai 2019</button>
			</form>			
        </li>
		
        <li>
            <h3>QCM sujet n° 1 - type BNS - Mars 2020</h3>
            <p>460;461;462;463;464;465;466;467;468;469;470;471;472;473;474;475;476;477;478;479;480;481;482;483;484;485;486;487;488;489;490;491;492;493;494;495;496;497;498;499;500;501</p>
            <form method="post" action="qcm-valide-cle.php">
			<input type="hidden"  
			value="460;461;462;463;464;465;466;467;468;469;470;471;472;473;474;475;476;477;478;479;480;481;482;483;484;485;486;487;488;489;490;491;492;493;494;495;496;497;498;499;500;501" 
			name="cle" >   
			<button class='btn btn-info'>Générer le QCM n° 1 - type BNS - Mars 2020</button>
			</form>
		</li>
		
        <li>
            <h3>QCM sujet n° 3 - type BNS - Mars 2020</h3>
            <p>913;914;915;916;917;918;919;920;921;922;923;924;925;926;927;928;929;930;931;932;933;934;935;936;937;938;939;940;941;942;943;944;945;946;947;948;949;950;951;952;953;954</p>
            <form method="post" action="qcm-valide-cle.php">
			<input type="hidden"  
			value="913;914;915;916;917;918;919;920;921;922;923;924;925;926;927;928;929;930;931;932;933;934;935;936;937;938;939;940;941;942;943;944;945;946;947;948;949;950;951;952;953;954" 
			name="cle" >   
			<button class='btn btn-info'>Générer le QCM n° 3 - type BNS - Mars 2020</button>
			</form>
		</li>		
		
        <li>
            <h3>QCM sujet n° 20 - type BNS - Mars 2020</h3>
            <p>577;578;579;580;581;582;583;584;585;586;587;588;589;590;591;592;593;594;595;596;597;598;599;600;601;602;603;604;605;606;607;608;609;610;611;612;613;614;615;616;617;618</p>
            <form method="post" action="qcm-valide-cle.php">
			<input type="hidden"  
			value="577;578;579;580;581;582;583;584;585;586;587;588;589;590;591;592;593;594;595;596;597;598;599;600;601;602;603;604;605;606;607;608;609;610;611;612;613;614;615;616;617;618" 
			name="cle" >   
			<button class='btn btn-info'>Générer le QCM n° 20 - type BNS - Mars 2020</button>
			</form>
		</li>		
    </ul>

<?php
endif;
?>

<?php include("footer.php") ?>

</body>

</html>


